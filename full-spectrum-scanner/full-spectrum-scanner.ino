// =============================================================================
// Full Spectrum Scanner (FSS) control panels
// EDSIC (Elite Dangerous Ships Integrated Control) Sub-Project
// EDSIS (Elite Dangerous Ships Integrated Systems) Project
// Copyright ©2023 David McMurray
// =============================================================================

// Pins
int b_actv = 2;   // FSS activate/deactivate
int i_actv = 3;
int b_stg1 = 4;   // Stage 1 - pre-charge containment grid
int i_stg1 = 5;
int b_stg2 = 6;   // Stage 2 - engage power coupling
int i_stg2 = 7;
int b_honk = 8;   // Engage system honk
int i_honk = 9;
int b_cncl = 10;  // Cancel all stages of system honk (1, 2 and engage)
int i_cncl = 11;

// State flags (functions)
bool ff_actv = false;
bool ff_cncl = false;

// State flags (buttons)
volatile bool fb_actv = false;
volatile bool fb_stg1 = false;
volatile bool fb_stg2 = false;
volatile bool fb_honk = false;
volatile bool fb_cncl = false;
volatile bool fbp_actv = false;
volatile bool fbp_stg1 = false;
volatile bool fbp_stg2 = false;
volatile bool fbp_honk = false;
volatile bool fbp_cncl = false;

// State flags (indicators)
bool fi_actv = false;
bool fi_stg1 = false;
bool fi_stg2 = false;
bool fi_honk = false;
bool fi_cncl = false;

// Timers
unsigned long tb_actv = 0;
unsigned long t_actv = 0;
unsigned long t_cncl = 0;

//Counters
int c_cncl = 0;

void setup() {
  // Buttons
  pinMode(b_actv,INPUT_PULLUP);
  pinMode(b_stg1,INPUT_PULLUP);
  pinMode(b_stg2,INPUT_PULLUP);
  pinMode(b_honk,INPUT_PULLUP);
  pinMode(b_cncl,INPUT_PULLUP);

  // Indicators
  pinMode(i_actv,OUTPUT);
  pinMode(i_stg1,OUTPUT);
  pinMode(i_stg2,OUTPUT);
  pinMode(i_honk,OUTPUT);
  pinMode(i_cncl,OUTPUT);

  // Init indicators
  digitalWrite(i_actv,LOW);
  digitalWrite(i_stg1,LOW);
  digitalWrite(i_stg2,LOW);
  digitalWrite(i_honk,LOW);
  digitalWrite(i_cncl,LOW);

  // Register interrupt service routines
  attachInterrupt(digitalPinToInterrupt(b_actv),Activate,FALLING);
  attachInterrupt(digitalPinToInterrupt(b_stg1),Stage1,FALLING);
  attachInterrupt(digitalPinToInterrupt(b_stg2),Stage2,FALLING);
  attachInterrupt(digitalPinToInterrupt(b_honk),Honk,FALLING);
  attachInterrupt(digitalPinToInterrupt(b_cncl),Cancel,FALLING);

  // Start serial comms
  Serial.begin(9600);
  Serial.println("FSS system initiated");
}

void loop() {
//  ReadButtons();
  
  CheckActvBtn();
  if (ff_actv) {
    CheckStg1Btn();
    CheckStg2Btn();
    CheckHonkBtn();
    CheckCnclBtn();
  }

  FlashActvInd();
  FlashCnclInd();
  
  ResetButtonFlags();
}

//void ReadButtons() {
//  bool c = (digitalRead(b_actv) == LOW);
//  fb_actv = (!fbp_actv && c) ? true : false;
//  fbp_actv = c;
//
//  c = (digitalRead(b_stg1) == LOW);
//  fb_stg1 = (!fbp_stg1 && c) ? true : false;
//  fbp_stg1 = c;
//
//  c = (digitalRead(b_stg2) == LOW);
//  fb_stg2 = (!fbp_stg2 && c) ? true : false;
//  fbp_stg2 = c;
//
//  c = (digitalRead(b_honk) == LOW);
//  fb_honk = (!fbp_honk && c) ? true : false;
//  fbp_honk = c;
//
//  c = (digitalRead(b_cncl) == LOW);
//  fb_cncl = (!fbp_cncl && c) ? true : false;
//  fbp_cncl = c;
//}

void CheckActvBtn() {
  if (tb_actv != 0 && millis()-tb_actv > 100) {
    tb_actv = 0;
    Serial.println("Activate pressed");
    ff_actv = !ff_actv;
    if (ff_actv) {
      fi_actv = true;
      digitalWrite(i_actv, fi_actv);
      t_actv = millis();
      Serial.println("FSS on");
    }
    else {
      // Activate
      fi_actv = false;
      digitalWrite(i_actv,fi_actv);
      t_actv = 0;
      // Stage 1
      fi_stg1 = false;
      digitalWrite(i_stg1,fi_stg1);
      // Stage 2
      fi_stg2 = false;
      digitalWrite(i_stg2,fi_stg2);
      // Honk
      fi_honk = false;
      digitalWrite(i_honk,fi_honk);
      // Cancel
      fi_cncl = false;
      digitalWrite(i_cncl,fi_cncl);
      Serial.println("FSS off");
    }
  }

//  if (fb_actv) {
//    Serial.println("Activate pressed");
//    ff_actv = !ff_actv;
//    if (ff_actv) {
//      fi_actv = true;
//      digitalWrite(i_actv, fi_actv);
//      t_actv = millis();
//      Serial.println("FSS on");
//    }
//    else {
//      // Activate
//      fi_actv = false;
//      digitalWrite(i_actv,fi_actv);
//      t_actv = 0;
//      // Stage 1
//      fi_stg1 = false;
//      digitalWrite(i_stg1,fi_stg1);
//      // Stage 2
//      fi_stg2 = false;
//      digitalWrite(i_stg2,fi_stg2);
//      // Honk
//      fi_honk = false;
//      digitalWrite(i_honk,fi_honk);
//      // Cancel
//      fi_cncl = false;
//      digitalWrite(i_cncl,fi_cncl);
//      Serial.println("FSS off");
//    }
//  }
}

void FlashActvInd() {
  if (ff_actv) {
    // Flash light when on
    if (fi_actv) {
      if (millis()-t_actv >= 1800) {
        fi_actv = false;
        digitalWrite(i_actv,fi_actv);
        t_actv = millis();
      }
    }
    else {
      if (millis()-t_actv >= 200) {
        fi_actv = true;
        digitalWrite(i_actv,fi_actv);
        t_actv = millis();
      }
    }
  }
}

void CheckStg1Btn() {
  if (fb_stg1 && !fi_stg1) {
    fi_stg1 = true;
    digitalWrite(i_stg1,fi_stg1);
    Serial.println("Stage 1 pressed");
  }
}

void CheckStg2Btn() {
  if (fb_stg2 && fi_stg1 && !fi_stg2) {
    fi_stg2 = true;
    digitalWrite(i_stg2,fi_stg2);
    Serial.println("Stage 2 pressed");
  }
}

void CheckHonkBtn() {
  if (fb_honk && fi_stg2 && !fi_honk) {
    fi_honk = true;
    digitalWrite(i_honk,fi_honk);
    Serial.println("Honk pressed");
  }
}

void CheckCnclBtn() {
  if (fb_cncl) {
    ff_cncl = true;
    fi_cncl = true;
    digitalWrite(i_cncl,fi_cncl);
    fi_honk = false;
    digitalWrite(i_honk,fi_honk);
    t_cncl = millis();
    c_cncl = 8;
    Serial.println("Cancel pressed");
  }
}

void FlashCnclInd() {
  if (ff_cncl) {
    if (fi_cncl) {
      if (millis()-t_cncl >= 100) {
        fi_cncl = false;
        digitalWrite(i_cncl,fi_cncl);
        t_cncl = millis();
        if (c_cncl == 0) {
          ff_cncl = false;
          t_cncl = 0;
        }
      }
    }
    else {
      if (millis()-t_cncl >= 100) {
        fi_cncl = true;
        digitalWrite(i_cncl,fi_cncl);
        switch (c_cncl) {
          case 6:
            fi_stg2 = false;
            digitalWrite(i_stg2,fi_stg2);
            break;
          case 3:
            fi_stg1 = false;
            digitalWrite(i_stg1,fi_stg1);
            break;
        }
        t_cncl = millis();
        c_cncl--;
      }
    }
  }
}

void ResetButtonFlags() {
  fb_actv = false;
  fb_stg1 = false;
  fb_stg2 = false;
  fb_honk = false;
  fb_cncl = false;
}

void Activate() {
//  fb_actv = (digitalRead(b_actv) == LOW);
  tb_actv = millis();
}

void Stage1() {
  fb_stg1 = (digitalRead(b_stg1) == LOW);
}

void Stage2() {
  fb_stg2 = (digitalRead(b_stg2) == LOW);
}

void Honk() {
  fb_honk = (digitalRead(b_honk) == LOW);
}

void Cancel() {
  fb_cncl = (digitalRead(b_cncl) == LOW);
}
