# EDSIC (Arduino) #

Copyright 2020 David McMurray

The contents of this repository are [GPL-3.0 Licensed](https://www.gnu.org/licenses/gpl-3.0.txt)

## Description ##

Arduino section of the EDSIS projects EDSIC development.